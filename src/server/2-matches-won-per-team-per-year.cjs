const csv = require("csv-parser");
const fileSystem = require("fs");

function matchesWonPerTeamPerYear() {
  const iplArrayResult = [];

  const outputFilePath = (__dirname, "./src/public/output/2-matches-won-per-team-per-year.json");
  const inputDataFilePath = (__dirname, "./src/data/matches.csv");

  fileSystem.createReadStream(inputDataFilePath)
    .pipe(csv())
    .on("data", (data) => iplArrayResult.push(data))
    .on("end", () => {
      const matchesWonPerYear = iplArrayResult.reduce(function (accumulator, currentValue) {
        if (accumulator.hasOwnProperty(currentValue.season)) {
          if (accumulator[currentValue.season].hasOwnProperty(currentValue.winner)) {
            accumulator[currentValue.season][currentValue.winner] += 1;
          } else {
            accumulator[currentValue.season][currentValue.winner] = 1;
          }
        } else {
          accumulator[currentValue.season] = {};
        }
        return accumulator;
      }, {})

      fileSystem.writeFile(outputFilePath, JSON.stringify(matchesWonPerYear), error => {
        if (error) {
          throw error;
        } else {
          console.log('File written successfully');
        }
      })
    });
}

matchesWonPerTeamPerYear();