const csv = require("csv-parser");
const fileSystem = require("fs");

function wonPlayerOfTheMatch() {
    const iplMatchesArray = [];

    const outputFilePath = (__dirname, "./src/public/output/6-won-player-of-the-match.json");
    const inputMatchesFilePath = (__dirname, "./src/data/matches.csv");

    fileSystem.createReadStream(inputMatchesFilePath)
        .pipe(csv())
        .on("data", (data) => iplMatchesArray.push(data))
        .on("end", () => {
            const playerOfTheMatch = iplMatchesArray.reduce((accumulator, currentValue) => {
                if (accumulator[currentValue.season]) {
                    if (accumulator[currentValue.season][currentValue.player_of_match]) {
                        accumulator[currentValue.season][currentValue.player_of_match] += 1;
                    } else {
                        accumulator[currentValue.season][currentValue.player_of_match] = 1;
                    }
                } else {
                    accumulator[currentValue.season] = {};
                    accumulator[currentValue.season][currentValue.player_of_match] = 1;
                }
                return accumulator;
            }, {})

            const listOfThePlayers = Object.entries(playerOfTheMatch)
                .reduce((accumulator, currentValue) => {
                    const perYear = Object.fromEntries(Object.entries(currentValue[1])
                        .sort((start, end) => end[1] - start[1])
                        .slice(0, 1));
                    accumulator[currentValue[0]] = perYear;
                    return accumulator;
                }, {})
    
            fileSystem.writeFile(outputFilePath, JSON.stringify(listOfThePlayers), error => {
                if (error) {
                    throw error;
                } else {
                    console.log('File written successfully');
                }
            })
        })
}

wonPlayerOfTheMatch();
