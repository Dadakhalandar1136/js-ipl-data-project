const csv = require("csv-parser");
const fileSystem = require("fs");

function bestEconomyInSuperOver() {
    const iplDeliveriesArray = [];

    const outputFilePath = (__dirname, "./src/public/output/9-bowler-with-best-economy-in-super-overs.json");
    const inputDiliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

    fileSystem.createReadStream(inputDiliveriesFilePath)
        .pipe(csv())
        .on("data", (data) => iplDeliveriesArray.push(data))
        .on("end", () => {
            const superOverArray = iplDeliveriesArray.filter((element) => {
                return element.is_super_over == '1';
            })
                .reduce((accumulator, currentValue) => {
                    if (currentValue.wide_runs == '0' && currentValue.noball_runs == '0') {
                        if (accumulator[currentValue.bowler]) {
                            accumulator[currentValue.bowler][0] += Number(currentValue.batsman_runs);
                            accumulator[currentValue.bowler][1] += 1;
                        } else {
                            accumulator[currentValue.bowler] = [Number(currentValue.batsman_runs), 1];
                        }
                    } else {
                        if (accumulator[currentValue.bowler]) {
                            accumulator[currentValue.bowler[0]] = Number(currentValue.total_runs) + 1;
                        } else {
                            accumulator[currentValue.bowler] = [Number(currentValue.total_runs), 0];
                        }
                    }
                    return accumulator;
                }, {})

            const bowlerEconomy = Object.entries(superOverArray)
                .map((element) => {
                    element[1] = Math.round(element[1][0] / (element[1][1] / 6));
                    return element;
                })
                .sort((start, end) => {
                    if (start[1] < end[1]) {
                        return -1;
                    } else {
                        return 1;
                    }
                });

            const bowlerEconomyResult = bowlerEconomy;
                
            fileSystem.writeFile(outputFilePath, JSON.stringify(bowlerEconomyResult[0]), error => {
                if (error) {
                    throw error;
                } else {
                    console.log('File written successfully');
                }
            })
        })
}

bestEconomyInSuperOver();
