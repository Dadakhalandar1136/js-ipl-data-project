const csv = require("csv-parser");
const fileSystem = require("fs");
const { start } = require("repl");

function highestPlayerDismissed() {
    const iplDeliveriesData = [];

    const outputFilePath = (__dirname, "./src/public/output/8-highest-player-dismissed.json");
    const inputDiliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

    fileSystem.createReadStream(inputDiliveriesFilePath)
        .pipe(csv())
        .on("data", (data) => iplDeliveriesData.push(data))
        .on("end", () => {
            const listOfDismissedPlayer = iplDeliveriesData.reduce((accumulator, currentValue) => {
                if (currentValue.player_dismissed) {
                    if (accumulator[currentValue.bowler]) {
                        if (accumulator[currentValue.bowler].hasOwnProperty(currentValue.batsman)) {
                            accumulator[currentValue.bowler][currentValue.batsman] += 1;
                        } else {
                            accumulator[currentValue.bowler][currentValue.batsman] = 1;
                        }
                    } else {
                        accumulator[currentValue.bowler] = {}
                    }
                }
                return accumulator;
            }, {})

            const bowlerArrayConvert = Object.entries(listOfDismissedPlayer);
            const bowlerDismissedArraySort = bowlerArrayConvert.reduce((accumulator, currentValue) => {
                const sortedPlayers = Object.entries(currentValue[1]).sort((start, end) => end[1] - start[1]);
                accumulator[currentValue[0]] = sortedPlayers[0];
                return accumulator;
            }, {})

            const bowlerDismissedBatsman = Object.fromEntries(Object.entries(bowlerDismissedArraySort)
                .filter(value => value[1])
                .sort((start, end) => end[1][1] - start[1][1]).slice(0, 1));

            fileSystem.writeFile(outputFilePath, JSON.stringify(bowlerDismissedBatsman), error => {
                if (error) {
                    throw error;
                } else {
                    console.log('File written successfully');
                }
            })
        })
}

highestPlayerDismissed();
