const csv = require("csv-parser");
const fileSystem = require("fs");

function topEconomicalBowlers() {
    const iplMatchesArray = [];
    const iplDeliveriesArray = [];

    const outputFilePath = (__dirname, "./src/public/output/4-top-economical-bowlers.json");
    const inputMatchesFilePath = (__dirname, "./src/data/matches.csv");
    const inputDiliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

    fileSystem.createReadStream(inputMatchesFilePath)
        .pipe(csv())
        .on("data", (data) => iplMatchesArray.push(data))
        .on("end", () => {
            const matchedYear2015 = iplMatchesArray.filter((element) => {
                return element.season == 2015;
            })
                .map((element) => {
                    return element.id;
                });

            fileSystem.createReadStream(inputDiliveriesFilePath)
                .pipe(csv())
                .on("data", (data) => iplDeliveriesArray.push(data))
                .on("end", () => {
                    const bowlersRuns = iplDeliveriesArray.reduce((accumulator, currentValue) => {
                        if (matchedYear2015.includes(currentValue.match_id)) {
                            if (accumulator[currentValue.bowler]) {
                                accumulator[currentValue.bowler] += Number(currentValue.total_runs);
                            } else {
                                accumulator[currentValue.bowler] = Number(currentValue.total_runs);
                            }
                        }
                        return accumulator;
                    }, {})

                    const bowlersBalls = iplDeliveriesArray.reduce((accumulator, currentValue) => {
                        if (matchedYear2015.includes(currentValue.match_id)) {
                            if (accumulator[currentValue.bowler]) {
                                currentValue.wide_runs == 0 && currentValue.noball_runs == 0 ? accumulator[currentValue.bowler] += 1 : accumulator[currentValue.bowler] += 0;
                            } else {
                                currentValue.wide_runs == 0 && currentValue.noball_runs == 0 ? accumulator[currentValue.bowler] = 1 : accumulator[currentValue.bowler] = 0;
                            }
                        }
                        return accumulator;
                    }, {})

                    const bowlerEconomy = Object.entries(iplDeliveriesArray.reduce((accumulator, currentValue) => {
                        if (matchedYear2015.includes(currentValue.match_id)) {
                            accumulator[currentValue.bowler] = Number((bowlersRuns[currentValue.bowler] / (bowlersBalls[currentValue.bowler] / 6)).toFixed(2));
                        }
                        return accumulator;
                    }, {}))

                    const topBowlerEconomy = Object.fromEntries(Object.entries(bowlerEconomy.sort((start, end) => {
                        if (start[1] < end[1]) {
                            return -1;
                        } else {
                            return 1;
                        }
                    })).slice(0, 10).map(economyRate => economyRate[1]));

                    fileSystem.writeFile(outputFilePath, JSON.stringify(topBowlerEconomy), error => {
                        if (error) {
                            throw error;
                        } else {
                            console.log('File written successfully');
                        }
                    })
                })
        });
}

topEconomicalBowlers();
