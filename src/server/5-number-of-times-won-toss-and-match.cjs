const csv = require("csv-parser");
const fileSystem = require("fs");

function wonTossAndMatch() {
    const iplMatchesArray = [];

    const outputFilePath = (__dirname, "./src/public/output/5-number-of-times-won-toss-and-match.json");
    const inputMatchesFilePath = (__dirname, "./src/data/matches.csv");

    fileSystem.createReadStream(inputMatchesFilePath)
        .pipe(csv())
        .on("data", (data) => iplMatchesArray.push(data))
        .on("end", () => {
            const teamsWonTossAndMatch = iplMatchesArray.reduce((accumulator, currentValue) => {
                if (currentValue.toss_winner === currentValue.winner) {
                    if (accumulator[currentValue.winner]) {
                        accumulator[currentValue.winner] += 1;
                    } else {
                        accumulator[currentValue.winner] = 1;
                    }
                }
                return accumulator;
            }, {})

            fileSystem.writeFile(outputFilePath, JSON.stringify(teamsWonTossAndMatch), error => {
                if (error) {
                    throw error;
                } else {
                    console.log('File written successfully');
                }
            })
        })
}

wonTossAndMatch();
