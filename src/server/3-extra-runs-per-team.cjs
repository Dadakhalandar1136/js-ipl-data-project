const csv = require("csv-parser");
const fileSystem = require("fs");

function extraRunsPerTeam() {
    const iplArrayMatches = [];
    const iplArrayDeliveries = [];

    const outputFilePath = (__dirname, "./src/public/output/3-extra-runs-per-team.json");
    const inputMatchesFilePath = (__dirname, "./src/data/matches.csv");
    const inputDiliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

    fileSystem.createReadStream(inputMatchesFilePath)
        .pipe(csv())
        .on("data", (data) => iplArrayMatches.push(data))
        .on("end", () => {
            const matchedYear2016 = iplArrayMatches.filter((element) => {
                return element.season == 2016;
            })
                .map((element) => {
                    return element.id;
                });

            fileSystem.createReadStream(inputDiliveriesFilePath)
                .pipe(csv())
                .on("data", (data) => iplArrayDeliveries.push(data))
                .on("end", () => {
                    const extraRunsPerTeam = iplArrayDeliveries.reduce((accumulator, currentValue) => {
                        if (matchedYear2016.includes(currentValue.match_id)) {
                            if (accumulator[currentValue.bowling_team]) {
                                accumulator[currentValue.bowling_team] += Number(currentValue.extra_runs);
                            } else {
                                accumulator[currentValue.bowling_team] = Number(currentValue.extra_runs);
                            }
                        }
                        return accumulator;
                    }, {})

                    fileSystem.writeFile(outputFilePath, JSON.stringify(extraRunsPerTeam), error => {
                        if (error) {
                            throw error;
                        } else {
                            console.log('File written successfully');
                        }
                    })
                })
        });
}

extraRunsPerTeam();