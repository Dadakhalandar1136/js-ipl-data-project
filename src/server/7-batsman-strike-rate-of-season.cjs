const csv = require("csv-parser");
const fileSystem = require("fs");

function batsmanStrikeRateOfSeason() {
    const iplMatchesData = [];
    const iplDeliveriesData = [];

    const outputFilePath = (__dirname, "./src/public/output/7-batsman-strike-rate-of-season.json");
    const inputMatchesFilePath = (__dirname, "./src/data/matches.csv");
    const inputDiliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

    fileSystem.createReadStream(inputMatchesFilePath)
        .pipe(csv())
        .on("data", (data) => iplMatchesData.push(data))
        .on("end", () => {
            const seasonMatchedId = iplMatchesData.reduce((accumulator, currentValue) => {
                accumulator[currentValue.id] = currentValue.season;
                return accumulator;
            }, {})

            fileSystem.createReadStream(inputDiliveriesFilePath)
                .pipe(csv())
                .on('data', (data) => iplDeliveriesData.push(data))
                .on("end", () => {
                    const strikeRateOfBatsman = iplDeliveriesData.reduce((accumulator, currentValue) => {
                        const seasonList = seasonMatchedId[currentValue.match_id];
                        if (accumulator[currentValue.batsman]) {
                            if (accumulator[currentValue.batsman].hasOwnProperty(seasonList)) {
                                accumulator[currentValue.batsman][seasonList].totalRuns += Number(currentValue.batsman_runs);
                                accumulator[currentValue.batsman][seasonList].totalBalls += 1;
                            } else {
                                accumulator[currentValue.batsman][seasonList] = {};
                                accumulator[currentValue.batsman][seasonList].totalRuns = Number(currentValue.batsman_runs);
                                accumulator[currentValue.batsman][seasonList].totalBalls = 1;
                            }
                        } else {
                            accumulator[currentValue.batsman] = {};
                            accumulator[currentValue.batsman][seasonList] = {};
                            accumulator[currentValue.batsman][seasonList].totalRuns = Number(currentValue.total_runs);
                            accumulator[currentValue.batsman][seasonList].totalBalls = 1;
                        }
                        return accumulator;
                    }, {})

                    let batsmanStrikeRate = {};
                    Object.keys(strikeRateOfBatsman).map(elementValue1 => {
                        let strikeRate = {};
                        Object.keys(strikeRateOfBatsman[elementValue1]).map(elementValue2 => {
                            let calculationOfStrikeRate = (((strikeRateOfBatsman[elementValue1][elementValue2].totalRuns / strikeRateOfBatsman[elementValue1][elementValue2].totalBalls) * 100).toFixed(2));
                            strikeRate[elementValue2] = Number(calculationOfStrikeRate);
                        })
                        batsmanStrikeRate[elementValue1] = strikeRate;
                    })

                    fileSystem.writeFile(outputFilePath, JSON.stringify(batsmanStrikeRate), error => {
                        if (error) {
                            throw error;
                        } else {
                            console.log('File written successfully');
                        }
                    })
                })
        })
}

batsmanStrikeRateOfSeason();
