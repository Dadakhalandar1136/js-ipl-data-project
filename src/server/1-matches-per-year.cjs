const csv = require('csv-parser')
const fileSystem = require('fs')

function matchesPerYear() {
    const results = [];

    const outputFilePath = (__dirname, './src/public/output/1-matches-per-year.json');
    const inputDataFilePath = (__dirname, './src/data/matches.csv');

    fileSystem.createReadStream(inputDataFilePath)
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', () => {
            const matchesPerYear = results.reduce(function (accumulator, currentValue) {
                if (accumulator[currentValue.season]) {
                    accumulator[currentValue.season] += 1;
                } else {
                    accumulator[currentValue.season] = 1;
                }
                return accumulator;
            }, {})

            fileSystem.writeFile(outputFilePath, JSON.stringify(matchesPerYear), error => {
                if (error) {
                    throw error;
                } else {
                    console.log('File written successfully');
                }
            })
        });
}

matchesPerYear();