fetch('./output/1-matches-per-year.json')
  .then(data => data.json())
  .then(data => {
    const matchesPerYear = Object.values(data);

    Highcharts.chart('container1', {
      chart: {
        type: 'area'
      },
      title: {
        text: 'Matches Per Year for IPL Project'
      },
      subtitle: {
        text: '* Missing data for season in 2018',
        align: 'right',
        verticalAlign: 'bottom'
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 60,
        floating: true,
        borderWidth: 3,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
      },
      yAxis: {
        title: {
          text: 'Matches'
        }
      },
      plotOptions: {
        series: {
          pointStart: 2008
        },
        area: {
          fillOpacity: 0.5
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Season',
        data: matchesPerYear
      },]
    });
  });

fetch('./output/2-matches-won-per-team-per-year.json')
  .then(data => data.json())
  .then(data => {
    const teamsToArrayconvert = Object.values(data).reduce((accumulator, currentValue) => {
      let storeTeams = Object.keys(currentValue).map((teamData) => {
        return teamData;
      })
        .filter((elementOfTeam) => {
          const notListOfTeams = accumulator.find((currentValue) => {
            return currentValue === elementOfTeam;
          });
          return notListOfTeams === undefined;
        });
      accumulator.push(...storeTeams);
      return accumulator;
    }, []);

    let listOfSeasons = 1;
    const matchesWonPerTeam = Object.values(data).reduce((accumulator, currentValue) => {
      Object.entries(currentValue).map(([nameOfTheTeam, allDataOfTeam]) => {
        const teamAtPresent = accumulator.find((currentValue) => {
          return currentValue.name === nameOfTheTeam;
        });
        if (teamAtPresent === undefined) {
          accumulator.push({
            name: nameOfTheTeam,
            data: [allDataOfTeam]
          });
        } else {
          teamAtPresent.data.push(allDataOfTeam);
        }
      });
      teamsToArrayconvert.map((nameTeam) => {
        const dataTeams = accumulator.find((element) => {
          return element.name === nameTeam;
        });
        if (dataTeams === undefined) {
          accumulator.push({
            name: nameTeam,
            data: [null],
          })
        } else {
          if (dataTeams.data.length !== listOfSeasons) {
            dataTeams.data.push(null);
          }
        }
      });
      listOfSeasons += 1;
      return accumulator;
    }, []);

    const listOfSeasonsPerYear = Object.keys(data);

    Highcharts.chart('container2', {
      title: {
        text: 'Matches Won Per Team for Each Season in IPL Project',
        align: 'center'
      },
      xAxis: {
        categories: listOfSeasonsPerYear
      },
      yAxis: {
        title: {
          text: 'Matches Won Per Season'
        }
      },
      tooltip: {
        valueSuffix: ' Matches'
      },
      series: matchesWonPerTeam
    });
  });

fetch('./output/3-extra-runs-per-team.json')
  .then(data => data.json())
  .then(data => {
    const extraRunsPerTeam = Object.entries(data);

    Highcharts.chart('container3', {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45
        }
      },
      title: {
        text: 'Extra Runs Per Team In IPL Project',
        align: 'center'
      },
      subtitle: {
        text: '',
        align: 'left'

      },
      plotOptions: {
        pie: {
          innerSize: 100,
          depth: 45
        }
      },
      series: [{
        name: 'Extra Runs ',
        data: extraRunsPerTeam
      }]
    });
  });


fetch('./output/4-top-economical-bowlers.json')
  .then(data => data.json())
  .then(data => {
    const bowlerKey = Object.keys(data);
    const bowlerValue = Object.values(data);

    // Set up the chart
    const chart = new Highcharts.Chart({
      chart: {
        renderTo: 'container4',
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 15,
          beta: 15,
          depth: 50,
          viewDistance: 25
        }
      },
      xAxis: {
        categories: bowlerKey
      },
      yAxis: {
        title: {
          enabled: false
        }
      },
      tooltip: {
        headerFormat: '<b>{point.key}</b><br>',
        pointFormat: 'Economic Rate : {point.y}'
      },
      title: {
        text: 'Top 10 Economical Bowlers In IPL Project',
        align: 'center'
      },
      subtitle: {
        text: 'Source: ' +
          '<a href="https://ofv.no/registreringsstatistikk"' +
          'target="_blank">OFV</a>',
        align: 'left'
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        column: {
          depth: 25
        }
      },
      series: [{
        data: bowlerValue,
        colorByPoint: true
      }]
    });
    chart;
  })

fetch('./output/5-number-of-times-won-toss-and-match.json')
  .then(data => data.json())
  .then(data => {
    const teamsAsKey = Object.keys(data);
    const wonMatchesAsValue = Object.values(data);

    Highcharts.chart('container5', {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Number of Times Each Team Won Toss and Won The Match In IPL Project'
      },
      subtitle: {
        text: ''

      },
      xAxis: {
        categories: teamsAsKey,
      },
      yAxis: {
        title: {
          text: 'Won Toss As Well Match'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [{
        name: 'Team Won Matches',
        data: wonMatchesAsValue,
      }]
    });
  })

fetch('./output/6-won-player-of-the-match.json')
  .then(data => data.json())
  .then(data => {
    const teamsToArrayconvert = Object.values(data).reduce((accumulator, currentValue) => {
      let storeTeams = Object.keys(currentValue).map((teamData) => {
        return teamData;
      })
        .filter((elementOfTeam) => {
          const notListOfTeams = accumulator.find((currentValue) => {
            return currentValue === elementOfTeam;
          });
          return notListOfTeams === undefined;
        });
      accumulator.push(...storeTeams);
      return accumulator;
    }, []);

    let listOfSeasons = 1;
    const playerOfTheMatch = Object.values(data).reduce((accumulator, currentValue) => {
      Object.entries(currentValue).map(([nameOfTheTeam, allDataOfTeam]) => {
        const teamAtPresent = accumulator.find((currentValue) => {
          return currentValue.name === nameOfTheTeam;
        });
        if (teamAtPresent === undefined) {
          accumulator.push({
            name: nameOfTheTeam,
            data: [allDataOfTeam]
          });
        } else {
          teamAtPresent.data.push(allDataOfTeam);
        }
      });
      teamsToArrayconvert.map((nameTeam) => {
        const dataTeams = accumulator.find((element) => {
          return element.name === nameTeam;
        });
        if (dataTeams === undefined) {
          accumulator.push({
            name: nameTeam,
            data: [null],
          })
        } else {
          if (dataTeams.data.length !== listOfSeasons) {
            dataTeams.data.push(null);
          }
        }
      });
      listOfSeasons += 1;
      return accumulator;
    }, []);

    const listOfSeasonsPerYear = Object.keys(data);

    Highcharts.chart('container6', {
      title: {
        text: 'Player of The Match for Each Season in IPL Project',
        align: 'center'
      },
      xAxis: {
        categories: listOfSeasonsPerYear,
      },
      yAxis: {
        title: {
          text: 'Player Of The Matches'
        }
      },
      tooltip: {
        valueSuffix: ' Player Of The Match'
      },
      series: playerOfTheMatch,
    });
  })

fetch('./output/7-batsman-strike-rate-of-season.json')
  .then(data => data.json())
  .then(data => {
    const teamsToArrayconvert = Object.values(data).reduce((accumulator, currentValue) => {
      let storeTeams = Object.keys(currentValue).map((teamData) => {
        return teamData;
      })
        .filter((elementOfTeam) => {
          const notListOfTeams = accumulator.find((currentValue) => {
            return currentValue === elementOfTeam;
          });
          return notListOfTeams === undefined;
        });
      accumulator.push(...storeTeams);
      return accumulator;
    }, []);

    let listOfSeasons = 1;
    const playerStrikeRate = Object.values(data).reduce((accumulator, currentValue) => {
      Object.entries(currentValue).map(([nameOfTheTeam, allDataOfTeam]) => {
        const teamAtPresent = accumulator.find((currentValue) => {
          return currentValue.name === nameOfTheTeam;
        });
        if (teamAtPresent === undefined) {
          accumulator.push({
            name: nameOfTheTeam,
            data: [allDataOfTeam]
          });
        } else {
          teamAtPresent.data.push(allDataOfTeam);
        }
      });
      teamsToArrayconvert.map((nameTeam) => {
        const dataTeams = accumulator.find((element) => {
          return element.name === nameTeam;
        });
        if (dataTeams === undefined) {
          accumulator.push({
            name: nameTeam,
            data: [null],
          })
        } else {
          if (dataTeams.data.length !== listOfSeasons) {
            dataTeams.data.push(null);
          }
        }
      });
      listOfSeasons += 1;
      return accumulator;
    }, []);

    const listOfTheKeyNames = Object.keys(data);

    Highcharts.chart('container7', {
      title: {
        text: 'Strike Rate of The Player for Each Season in IPL Project',
        align: 'center'
      },
      xAxis: {
        categories: listOfTheKeyNames,
      },
      yAxis: {
        title: {
          text: 'Strike Rates'
        }
      },
      tooltip: {
        valueSuffix: ' Strike Rate'
      },
      series: playerStrikeRate,
    });
  })

fetch('./output/8-highest-player-dismissed.json')
  .then(data => data.json())
  .then(data => {
    const playerDismissedValue = Object.entries(data)
    const playerKey = Object.keys(data)

    Highcharts.chart('container8', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Highest Number of Times One Player Has Been Dismissed by Another Player in IPL Project'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: playerKey,
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: ''
      },
      series: [{
        name: 'Z Khan',
        data: playerDismissedValue[0],
        dataLabels: {
          enabled: true,
          rotation: '',
          color: '#FFFFFF',
          align: 'center',
          format: '{point.y:.f}', // one decimal
          y: 20, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      }]
    });
  })

fetch('./output/9-bowler-with-best-economy-in-super-overs.json')
  .then(data => data.json())
  .then(data => {
    const bestEconomyPlayer = Object.entries(Object.fromEntries([data]));

    Highcharts.chart('container9', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Bowler With The Best Economy in Super Overs in IPL Project'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: '',
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: ''
      },
      series: [{
        name: '',
        data: bestEconomyPlayer,
        dataLabels: {
          enabled: true,
          rotation: '',
          color: '#FFFFFF',
          align: 'center',
          format: '{point.y:.f}', // one decimal
          y: 20, // 10 pixels down from the top
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
          }
        }
      }]
    });
  })